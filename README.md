# spin-rotator

spin-rotator is a small application designed to calculate the precession of a spin in
a magnetic field.

## Requirements

sympy 1.5.1  
matplotlib 3.2.1  
numpy 1.18.1  

## Installation

No installation required

## Usage

syntax in command line :  
`python3 spin.py {theta} {phi} {spin_orientation}`  
`theta` and `phi` are the angles in spherical coordinates that define the orientation of the magnetic field B in the following way : 
```math
B_x = \sin(\theta) \cos(\phi)\\
B_y = \sin(\theta) \sin(\phi)\\
B_z = \cos(\theta)\\
```
`spin_orientation` is the initial orientation of the spin S which can take the values x y or z.  
The application will output a tex file containing all the symbolic calculation in the selected configuration of magnetic
field and spin. The tex file can be compiled into a pdf using texmaker or similar. A 3D graph is also generated to show
the time evolution of the mean spin around the magnetic field.

examples :   
`python3 spin.py pi/4 3*pi/2 z`  
Generated picture :  
![](EXAMPLES_OUTPUTS/precession.png)
  
The generated tex is available here:  
[TEX FILE](EXAMPLES_OUTPUTS/fichier.tex)  
And the associated PDF here :  
[PDF FILE](EXAMPLES_OUTPUTS/fichier.pdf)
